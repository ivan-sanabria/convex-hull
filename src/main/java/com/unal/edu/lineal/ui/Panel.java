/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.ui;

import com.unal.edu.lineal.model.Point;
import com.unal.edu.lineal.processor.ConvexHull;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Random;

/**
 * Class to handle the draw elements of the convex hull.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class Panel extends JPanel implements MouseListener {

    /**
     * Interval to update the repaint after test is triggered in milliseconds.
     */
    private static final int UPDATE_PERIOD = 50;

    /**
     * Define logger to track errors and show messages.
     */
    private static Logger LOG = LogManager.getLogger(Panel.class);

    /**
     * Generates random colors to display convex hull.
     */
    private static Random GENERATOR = new Random();

    /**
     * Point of the click inside the panel.
     */
    private Point point;

    /**
     * Processor to get the convex hull.
     */
    private ConvexHull convexHull;

    /**
     * Constructor with the given dimensions.
     *
     * @param width  Width of the panel.
     * @param height Height of the panel.
     */
    Panel(final int width, final int height) {

        convexHull = new ConvexHull();

        setSize(width, height);
        addMouseListener(this);
        setBackground(Color.WHITE);
    }

    /**
     * Simulates the number of tests given as parameter.
     *
     * @param tests  Number of tests to simulate.
     * @param width  Width of the window is containing the panel.
     * @param height Height of the window is containing the panel.
     */
    void simulateTests(final int tests, final int width, final int height) {

        Thread simulate = new Thread(() -> {

            Random random = new Random();

            while (getPointSize() < tests) {

                final int x = random.nextInt(height - 10) + 5;
                final int y = random.nextInt(width - 60) + 5;

                addPoint(new Point(x, y));

                try {
                    // Delay and give other thread a chance to run.
                    Thread.sleep(UPDATE_PERIOD);

                } catch (InterruptedException ignore) {

                    LOG.error("Error waiting for next test.");
                }
            }
        });

        simulate.start();
    }

    /**
     * Retrieves the number of points in the set that are painted in the current panel.
     *
     * @return Number of points displayed on the panel.
     */
    int getPointSize() {
        return convexHull.getPointSize();
    }

    /**
     * Reset the draw panel without points.
     */
    void reset() {
        convexHull = new ConvexHull();
    }

    /**
     * When the user click on the panel the application graphics a point.
     *
     * @param e even given by the mouse.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        addPoint(
                new Point(
                        e.getX(),
                        e.getY()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(MouseEvent e) {
        LOG.debug("Override the method to track events.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        LOG.debug("Override the method to track events.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        LOG.debug("Override the method to track events.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(MouseEvent e) {
        LOG.debug("Override the method to track events.");
    }

    /**
     * Update the content of the panel after each click.
     *
     * @param g Graphics to update the content of the panel.
     */
    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        clean(g);

        final Color color = generateColor();

        for (int i = 0; i < convexHull.getPointSize(); i++) {

            final Point current = convexHull.getPoints()
                    .get(i);

            drawPoint(g, current.getX(), current.getY(), color);
        }

        if (point != null) {
            drawPoint(g, point.getX(), point.getY(), Color.RED);
            convexHull.addPoint(point);
        }

        drawConvexHull(g, convexHull.getConvexHull());
    }

    /**
     * Add a point to the UI.
     *
     * @param input point to add to the UI.
     */
    private void addPoint(final Point input) {
        point = input;
        repaint();
    }

    /**
     * Generate a random color to display the points, axes and the convex hull.
     *
     * @return A Color to paint the elements on the draw panel.
     */
    private Color generateColor() {

        final float cr = GENERATOR.nextFloat();
        final float cg = GENERATOR.nextFloat();
        final float cb = GENERATOR.nextFloat();

        return new Color(cr, cg, cb);
    }

    /**
     * Clean the screen of the panel.
     *
     * @param g Graphics to update the content of the panel.
     */
    private void clean(final Graphics g) {

        final Dimension d = getSize();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, d.width, d.height);

        drawAxis(g);
    }

    /**
     * Paint the axis over the panel.
     *
     * @param g Graphics to update the content of the panel.
     */
    private void drawAxis(final Graphics g) {

        final Color color = generateColor();

        g.setColor(color);
        g.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
        g.setColor(color);
        g.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight());
    }

    /**
     * Paint a point using the given color and coordinates.
     *
     * @param g     Graphics to update the content of the panel.
     * @param x     X value of the point.
     * @param y     Y value of the point.
     * @param color Color of the point.
     */
    private void drawPoint(final Graphics g, final int x, final int y, final Color color) {
        g.setColor(color);
        g.fillOval(x, y, 3, 3);
    }

    /**
     * Paint the convex hull using the given list of points.
     *
     * @param g      Graphics to update the content of the panel.
     * @param convex A List of points representing the convex hull of the set.
     */
    private void drawConvexHull(final Graphics g, final List<Point> convex) {

        final Color color = generateColor();

        if (convex.size() > 2) {

            final int[] x = new int[convex.size()];
            final int[] y = new int[convex.size()];

            for (int i = 0; i < convex.size(); i++) {
                x[i] = convex.get(i).getX();
                y[i] = convex.get(i).getY();
            }

            g.setColor(color);
            g.drawPolygon(x, y, convex.size());

        } else if (convex.size() == 2) {

            g.setColor(color);
            g.drawLine(convex.get(0).getX(), convex.get(0).getY(), convex.get(1).getX(), convex.get(1).getY());
        }
    }

}
