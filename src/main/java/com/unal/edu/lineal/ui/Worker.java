/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.ui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

/**
 * Class to implement swing worker in order to display tests in UI.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class Worker extends SwingWorker<Void, Void> {

    /**
     * Define logger to track errors.
     */
    private static final Logger LOG = LogManager.getLogger(Worker.class);

    /**
     * Panel to draw the points and test the convex hull.
     */
    private Panel graphics;

    /**
     * Text field to receive the number of points to try.
     */
    private JTextField points;

    /**
     * Button to start the convex hull process.
     */
    private JButton test;

    /**
     * Number of tests to simulate on convex hull process.
     */
    private int tests;

    /**
     * Constructor of the worker in order to display tests in UI.
     *
     * @param graphics Panel to draw the points and test the convex hull.
     * @param points   TextField component to capture the number of tests to trigger.
     * @param test     Button to trigger the convex hull process.
     * @param tests    Number of tests to simulate on convex hull process.
     */
    Worker(Panel graphics, JTextField points, JButton test, int tests) {
        this.graphics = graphics;
        this.points = points;
        this.test = test;
        this.tests = tests;
    }

    /**
     * Updates the UI in the background.
     */
    @Override
    protected Void doInBackground() {

        while (tests > graphics.getPointSize()) {
            LOG.debug("Number of points in the Draw Panel {}.", graphics.getPointSize());
        }

        points.setEnabled(true);
        test.setEnabled(true);

        return null;
    }

}
