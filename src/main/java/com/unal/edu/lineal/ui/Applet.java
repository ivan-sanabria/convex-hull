/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.ui;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;

/*
<applet code="Applet" width=600 height=600>
</applet>
*/

/**
 * Class to handle the applet of the convex hull.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@SuppressWarnings("deprecation")
public class Applet extends java.applet.Applet {

    /**
     * Width of the applet.
     */
    private static final int WIDTH = 600;

    /**
     * Height of the applet.
     */
    private static final int HEIGHT = 600;

    /**
     * Init the applet UI.
     */
    @Override
    public void init() {

        //Define applet and UI components.
        setSize(WIDTH, HEIGHT);

        // Define panel to paint simulation of convex hull.
        Panel graphics = new Panel(WIDTH, HEIGHT);

        // Define panel to add input components.
        JPanel panel = new JPanel();
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);

        JLabel label = new JLabel("Number of points: ");
        panel.add(label);

        JTextField points = new JTextField();
        points.setPreferredSize(new Dimension(400, 20));
        panel.add(points);

        JButton test = new JButton("Test");
        test.addActionListener(new Listener(graphics, points, test, WIDTH, HEIGHT));
        panel.add(test);

        // Add all the elements to simulate convex hull.
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.NORTH);
        this.add(graphics, BorderLayout.CENTER);
    }

}
