/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.ui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class to implement the action listener to trigger to convex hull process.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class Listener implements ActionListener {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(Listener.class);

    /**
     * Panel to draw the points and test the convex hull.
     */
    private Panel graphics;

    /**
     * Text field to receive the number of points to try.
     */
    private JTextField points;

    /**
     * Button to start the convex hull process.
     */
    private JButton test;

    /**
     * Width of the Applet or Frame that uses the Listener.
     */
    private int width;

    /**
     * Height of the Applet or Frame that uses the Listener.
     */
    private int height;

    /**
     * Constructor of the listener in order to trigger the convex hull process.
     *
     * @param graphics Panel to draw the points and test the convex hull.
     * @param points   TextField component to capture the number of tests to trigger.
     * @param test     Button to trigger the convex hull process.
     * @param width    Width of the Applet or Frame that uses the Listener.
     * @param height   Height of the Applet or Frame that uses the Listener.
     */
    Listener(final Panel graphics, final JTextField points, final JButton test, final int width, final int height) {
        this.graphics = graphics;
        this.points = points;
        this.test = test;
        this.width = width;
        this.height = height;
    }

    /**
     * Implements the action listener to trigger the convex hull process.
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        final String number = points.getText()
                .trim();

        try {

            final int tests = Integer.parseInt(number);
            final int maximum = (width * height / 2);

            if (tests > 0 && tests < maximum) {

                graphics.reset();
                graphics.simulateTests(tests, width, height);

                points.setEnabled(false);
                test.setEnabled(false);

                final Worker checker = new Worker(graphics, points, test, tests);
                checker.execute();
            }

        } catch (NumberFormatException nfe) {

            LOG.error("Error casting the number {}.", number);
            points.setText("");
        }
    }

}
