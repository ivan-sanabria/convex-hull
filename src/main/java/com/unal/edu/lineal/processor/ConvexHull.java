/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.processor;

import com.unal.edu.lineal.model.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class to search the convex hull of the list.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ConvexHull {

    /**
     * Define the list of points to build the convex hull.
     */
    private final List<Point> points = new ArrayList<>();

    /**
     * Add the given point to the list.
     *
     * @param point Point to add into the list.
     */
    public void addPoint(final Point point) {
        points.add(point);
    }

    /**
     * Get the current list of points.
     *
     * @return A List representing the points to build the convex hull.
     */
    public List<Point> getPoints() {
        return this.points;
    }

    /**
     * Retrieves the size of the list in the current instance.
     *
     * @return Size of points in the current instance.
     */
    public int getPointSize() {
        return this.points.size();
    }

    /**
     * Retrieves the convex hull of the points added to the current instance.
     *
     * @return A List with the points which belongs to the convex hull.
     */
    public List<Point> getConvexHull() {

        final ArrayList<Point> convex = new ArrayList<>();

        if (getPointSize() > 1) {

            Collections.sort(points);

            for (int i = 0; i < getPointSize(); ++i) {
                searchConvex(convex, i);
            }

            final ArrayList<Point> upper = new ArrayList<>();

            for (int i = getPointSize() - 1; i >= 0; i--) {
                searchConvex(upper, i);
            }

            convex.remove(convex.size() - 1);
            upper.remove(upper.size() - 1);

            convex.addAll(upper);

        } else if (getPointSize() == 1) {

            convex.add(this.points.get(0));
        }

        return convex;
    }

    /**
     * List of points to add to the list.
     *
     * @param points List of points to add into the list.
     */
    void addPoints(final List<Point> points) {
        points.forEach(this::addPoint);
    }

    /**
     * Add points to the convex hull, could be upper or lower convex hull.
     *
     * @param convex Convex hull point list.
     * @param i      Index to evaluate cross product.
     */
    private void searchConvex(ArrayList<Point> convex, int i) {

        int currentSize = convex.size();

        while (currentSize >= 2 && cross(convex.get(currentSize - 2), convex.get(currentSize - 1), this.points.get(i)) <= 0) {
            convex.remove(currentSize - 1);
            currentSize--;
        }

        convex.add(this.points.get(i));
    }

    /**
     * Calculates the cross product of the three points.
     *
     * @param first  First Point to calculate the cross product.
     * @param second Second Point to calculate the cross product.
     * @param third  Third Point to calculate the cross product.
     * @return A number representing the cross product between the 3 points.
     */
    private long cross(final Point first, final Point second, final Point third) {
        return (long) (second.getX() - first.getX()) * (third.getY() - first.getY()) -
                (long) (second.getY() - first.getY()) * (third.getX() - first.getX());
    }

}
