/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class to handle the point information to get the convex hull.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@AllArgsConstructor
@Data
public class Point implements Comparable<Point> {

    /**
     * X coordinate of the point.
     */
    private int x;

    /**
     * Y coordinate of the point.
     */
    private int y;

    /**
     * Compare two instances of Point using the given values.
     *
     * @param other Other instance to compare the current Point instance.
     * @return A number representing the result of compare.
     */
    @Override
    public int compareTo(Point other) {

        if (this.getX() == other.getX())
            return this.getY() - other.getY();
        else
            return this.getX() - other.getX();
    }

}
