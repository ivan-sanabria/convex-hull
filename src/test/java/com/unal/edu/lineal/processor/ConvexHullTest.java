/*
 * Copyright (C) 2010 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.unal.edu.lineal.processor;

import com.unal.edu.lineal.model.Point;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Class to manage the test case, using a specific set of points to validate the convex hull result.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ConvexHullTest {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(ConvexHullTest.class);


    @Test
    public void testConvexHullWithNoPoints() {

        final ConvexHull convexHull = new ConvexHull();

        final List<Point> convex = convexHull.getConvexHull();
        assertEquals(convex.size(), 0);
    }

    @Test
    public void testConvexHullWithSinglePoint() {

        final Point point = new Point(1, 1);

        final ConvexHull convexHull = new ConvexHull();
        convexHull.addPoint(point);

        final List<Point> convex = convexHull.getConvexHull();

        assertEquals(convex.size(), 1);
        assertEquals(convex.get(0), point);
    }

    @Test
    public void testConvexHullWithShortSet() {

        final List<Point> expected = new ArrayList<>();

        expected.add(new Point(0, 0));
        expected.add(new Point(6, 0));
        expected.add(new Point(1, 5));

        final ConvexHull convexHull = new ConvexHull();

        convexHull.addPoint(new Point(0, 0));
        convexHull.addPoint(new Point(1, 1));
        convexHull.addPoint(new Point(2, 2));
        convexHull.addPoint(new Point(1, 5));
        convexHull.addPoint(new Point(6, 0));
        convexHull.addPoint(new Point(4, 0));

        final List<Point> convex = convexHull.getConvexHull();
        assertEquals(convex.size(), 3);

        for (int a = 0; a < expected.size(); a++) {
            assertEquals(convex.get(a), expected.get(a));
        }
    }

    @Test
    public void testConvexHullWithLargeSet() {

        final List<Point> points = readPointData("./data/test-convex-data.txt");
        final List<Point> expected = readPointData("./data/test-convex-hull.txt");

        final ConvexHull convexHull = new ConvexHull();
        convexHull.addPoints(points);

        convexHull.getPoints()
                .forEach(p -> assertTrue(points.contains(p)));

        final List<Point> convex = convexHull.getConvexHull();
        assertEquals(convex.size(), expected.size());

        for (int a = 0; a < expected.size(); a++) {
            assertEquals(convex.get(a), expected.get(a));
        }
    }

    @Test
    public void testConvexHullWithMultipleCalls() {

        final ConvexHull convexHull = new ConvexHull();

        final List<Point> convexZero = convexHull.getConvexHull();
        assertEquals(convexZero.size(), 0);

        convexHull.addPoint(new Point(1, 1));

        final List<Point> convexOne = convexHull.getConvexHull();
        assertEquals(convexOne.size(), 1);

        convexHull.addPoint(new Point(1, 4));

        final List<Point> convexTwo = convexHull.getConvexHull();
        assertEquals(convexTwo.size(), 2);
    }

    /**
     * Read points data for the given location.
     *
     * @param location Location of the points data. Usually is comma separated value file with .txt extension.
     * @return A List of points found in the given file location.
     */
    private List<Point> readPointData(final String location) {

        final List<Point> expected = new ArrayList<>();
        final Path path = Paths.get(location);

        try {

            final BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
            String line;

            while ((line = reader.readLine()) != null) {

                final String[] coordinates = line.split(",");
                final Point point = new Point(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]));

                expected.add(point);
            }

        } catch (IOException iex) {

            LOG.error("Error reading the test convex - ", iex);
        }

        return expected;
    }

}
