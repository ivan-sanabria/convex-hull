# Convex Hull - University Project

version 1.1.0 - 23/11/2021

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/convex-hull.svg)](http://bitbucket.org/ivan-sanabria/convex-hull/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/convex-hull.svg)](http://bitbucket.org/ivan-sanabria/convex-hull/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_convex-hull&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_convex-hull)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_convex-hull&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_convex-hull)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_convex-hull&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_convex-hull)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_convex-hull&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_convex-hull)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_convex-hull&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_convex-hull)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_convex-hull&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_convex-hull)

## Introduction

Convex hull is known problem at university students in the career of computer science for further information please
look at the [documentation](https://en.wikipedia.org/wiki/Convex_hull).

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Frame.java**.

## Running Application on Terminal

To run the application on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean compile assembly:single
    java -jar target/convex-hull-1.1.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage on Terminal using Jacoco

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licences

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

# Contact Information

Email: icsanabriar@googlemail.com